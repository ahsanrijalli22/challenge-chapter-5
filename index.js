const express = require('express')
const morgan = require('morgan')
const app = express()
const logger = require('./middleware');
const port = process.env.PORT || 3000;

let users = require("./db/user.json");


app.use(express.json());
app.set('view engine', 'ejs')
app.use(express.static('public'))
app.use(express.static('views'))
app.use('/css',express.static(__dirname + 'public/css'))
app.use('/js',express.static(__dirname + 'public/js'))
app.use('/img',express.static(__dirname + 'public/img'))
app.use(express.urlencoded({
  extended: false,
}))



//middleware
app.use(morgan('dev'))
app.use(logger)

//Router 
app.get("/", (req, res) => {
      res.render('index',{ title: 'Home Page'});
}) 

app.get("/home", (req, res) => {
  res.render('index',{ title: 'Home Page'})
}) 

app.get("/login", (req,res) => {
    res.render('login',{title: 'Login Page'});
});

app.get("/signup", (req,res) => {
  res.render('signup',{title: 'Sign up Page'});
});
app.get("/game", (req,res) => {
  res.render("game");
});

app.get("/work", (req,res) => {
  res.render('work',{title: 'Work Page'});
});

app.get("/about", (req,res) => {
  res.render('about',{title: 'About Us'});
});

app.get("/contact", (req,res) => {
  res.render('contact',{title: 'Contact Page'});
});

app.post("/api/v1/user/login/",(req,res) => {
  const { username, password } = req.body
  if(users.find(i => i.username) === username &&
   users.find(i => i.password) === password) {
    res.render('game')
  } else {
    res.render('login',{ title: 'Login Page'})
  }


})  



app.listen(port, () => console.log("Server Running at http://localhost:3000"))